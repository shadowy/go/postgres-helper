package posgreshelper

import (
	"encoding/json"
	"reflect"
)

func ToJSON(search interface{}) interface{} {
	if search == nil {
		return nil
	}
	if reflect.ValueOf(search).IsNil() {
		return nil
	}
	jsonData, _ := json.Marshal(search)
	res := string(jsonData)
	return res
}
