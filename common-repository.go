package posgreshelper

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/application-settings/settings"
	"gitlab.com/shadowy/go/postgres"
)

type CommonRepository struct {
	config *settings.Configuration
}

func (repository *CommonRepository) Init(config *settings.Configuration) {
	repository.config = config
}

func (repository *CommonRepository) GetDB() *postgres.Database {
	if config, ok := repository.config.Config().(Configuration); ok {
		return config.GetDB()
	}

	logrus.Error("db.CommonRepository.getDB not support interface db.Configuration")

	return nil
}
