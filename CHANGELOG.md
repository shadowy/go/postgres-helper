# CHANGELOG

<!--- next entry here -->

## 0.2.2
2020-02-28

### Fixes

- fix add check for nil (7e99db4a255db60f285c70ae487487d709473b55)

## 0.2.1
2020-02-28

### Fixes

- convert to json (046505ed01e14faf6e6d6c11694cb98821fe9c6e)

## 0.2.0
2020-02-02

### Features

- update README.md (07a1f43f9dc3af05e3052293c9b91a77e6c99eb7)

## 0.1.1
2020-02-02

### Fixes

- name of package (95e578e27396bede12f0f534cc84ba6383a403be)

## 0.1.0
2020-02-02

### Features

- add common repository and utils (f4f103636bc09fda457b3aac40bb395e5f509565)