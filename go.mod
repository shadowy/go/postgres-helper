module gitlab.com/shadowy/go/postgres-helper

go 1.13

require (
	github.com/sirupsen/logrus v1.4.2
	gitlab.com/shadowy/go/application-settings v1.0.12
	gitlab.com/shadowy/go/postgres v1.0.12
)
